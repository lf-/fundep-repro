{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Database.Persist.Sqlite
import Database.Persist.TH
import Database.Esqueleto.Experimental
import qualified Database.Esqueleto.Experimental as E
import Data.Time.Clock
import Data.Text (Text)
import Control.Monad.IO.Class

share [mkPersist sqlSettings, mkMigrate "migrateBase"] [persistLowerCase|
LineItem
  name Text
  occurred UTCTime
  deriving Show

Something
  text Text
  lineItemId LineItemId
|]

migrateAll :: Migration
migrateAll = do
    migrateBase
    addMigration False "CREATE INDEX IF NOT EXISTS index_occurred ON line_item (occurred)"

typeError :: MonadIO m => SqlPersistT m (Maybe (Value Text))
typeError = selectOne $ do
    (li :& sg) <- from $ table @LineItem
        `leftJoin` table @Something
        `on` (\(li :& sg) -> just (li ^. #id) E.==. sg ?. #lineItemId)
    pure $ li ^. #name


main :: IO ()
main = do
    pure ()
